# DEFINICIONES:
class Planet
  CIRCLE_degrees = 360
  attr_reader :degrees, :distance, :angular_position, :cartesian_position, :degrees_in_radians

   def initialize(degrees, distance)
      @degrees = degrees.to_f
      @distance = distance  # Es el modulo con respecto al origen(el sol)
      @angular_position = 0.0
      @degrees_in_radians = to_rad(@angular_position)
      @cartesian_position = [@distance, 0.0]
   end

    def to_rad(degrees)
      degrees * Math::PI / 180.0
    end

   def advance_daily_position	
    increase_angular_position
    update_cartesian_position
   end

  def increase_angular_position
    @angular_position += @degrees
    if (@angular_position >= CIRCLE_degrees)
      @angular_position -= CIRCLE_degrees
    end
    update_degrees_in_radians
  end

  def update_degrees_in_radians
    @degrees_in_radians = to_rad(@angular_position)
  end

  def update_cartesian_position
    @cartesian_position = polar_to_cartesian(@distance, @degrees_in_radians)
  end

  def polar_to_cartesian(module_, degrees_in_radians)
    x = module_ * Math.cos(degrees_in_radians).round(5) 
    y = module_ * Math.sin(degrees_in_radians).round(5) 
    return [x, y]
  end

   def to_orbit(days)
    days.times do 
      advance_daily_position
    end
   end
end

# Para que un astro este alineado, necesita estar en el mismo grado o en el grado opuesto 
# VER COMO DETERMINAR ALINEAMIENTO
# Caso Planetas Alineados consigo y tambien con el sol


# Para que los astros esten alineados no tienen que formar un triangulo
# Para formar un triangulo, se debe comprobar que cumplan con la ley de los triangulos (la
# suma dos de sus lados tiene que ser mayor al lado restante)

# 1)SEQUIA : Todos los astros alineados (Incluyendo el sol) 
# 2)LLUVIA: Planetas forman un triangulo con el sol adentro de el triangulo
# 3)LLUVIA MAXIMA: Planetas forman un triangulo (TRIANGULO MAXIMO) con el sol adentro de el triangulo
# 4)CONDICIONES IDEALES: Planetas alineados entre si pero NO CON EL SOL

# Crear un simulador de planetas en movimiento el cual haga que se muevan cada dia e indiquen su 
# posicion actual segun el dia
# En un periodo de 10 años (365 dias por 10) detectar SEQUIA, LLUVIA, LLUVIA MAXIMA; CONDICIONES IDEALES

# Cuando se guarden estos casos especiales, indicar dia especifico y cantidad en la que ocurren (un acumulador 
# por caso)


# IMPLEMENTACION:

ferengi = Planet.new(1, 500)
betasoide = Planet.new(3, 2000)
vulcano = Planet.new(-5, 1000)

def show_planet(planet)
  puts "Posicion Angular #{planet.angular_position}"
  puts "Posicion Cartesiana (#{planet.cartesian_position[0]}, #{planet.cartesian_position[1]})"
end

def see_translation(planet, days_range, repetitions)
 repetitions.times do |repetition|
  puts "Dia #{days_range * repetition}"
  planet.to_orbit(days_range)
  show_planet(planet)
  end
end

#see_translation(betasoide, 4, 30)

# Ferengi

# # Dia Cero
# puts "Dia Cero"
# show_planet(betasoide)

# puts "Dia 1"
  betasoide.to_orbit(90)
 show_planet(betasoide)

# puts "Dia 30"
# betasoide.to_orbit()
# show_planet(betasoide)

# puts "Dia 60"
#  betasoide.to_orbit(30)
# show_planet(betasoide)

# puts "Dia 120"
#  betasoide.to_orbit(30)
# show_planet(betasoide)

# puts "Dia 150"
#  betasoide.to_orbit(30)
# show_planet(betasoide)

# puts "Dia 180"
#  betasoide.to_orbit(30)
# show_planet(betasoide)

# puts "Dia 210"
#  betasoide.to_orbit(30)
# show_planet(betasoide)

# puts "Dia 240"
#  betasoide.to_orbit(30)
# show_planet(betasoide)

# puts "Dia 270"
#  betasoide.to_orbit(30)
# show_planet(betasoide)

# puts "Dia 300"
#  betasoide.to_orbit(30)
# show_planet(betasoide)

# puts "Dia 330"
#  betasoide.to_orbit(30)
# show_planet(betasoide)

# puts "Dia 360"
#  betasoide.to_orbit(30)
# show_planet(betasoide)




