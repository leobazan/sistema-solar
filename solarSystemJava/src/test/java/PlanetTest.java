package test.java;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import main.java.solarSystem.Planet;

public class PlanetTest {
    double deltaD = 0;
    String deltaS = "";

    @Test
    public void when_orbit_updates_angular_position() {
	Planet planet = new Planet("", 9, 1);

	assertEquals(0, planet.getAngularPosition(), deltaD);

	planet.toOrbit(10);
	assertEquals(90, planet.getAngularPosition(), deltaD);

	planet.toOrbit(10);
	assertEquals(180, planet.getAngularPosition(), deltaD);

	planet.toOrbit(10);
	assertEquals(270, planet.getAngularPosition(), deltaD);

	planet.toOrbit(10);
	assertEquals(0, planet.getAngularPosition(), deltaD);
    }

    @Test
    public void when_orbit_updates_cartesian_position() {
	Planet planet = new Planet("", 9, 1);

	assertEquals("(1.0, 0.0)", planet.getCartesianPositionString());

	planet.toOrbit(10);
	assertEquals("(0.0, 1.0)", planet.getCartesianPositionString());

	planet.toOrbit(10);
	assertEquals("(-1.0, 0.0)", planet.getCartesianPositionString());

	planet.toOrbit(10);
	assertEquals("(0.0, -1.0)", planet.getCartesianPositionString());
    }

    @Test
    public void restart_degrees_when_it_passes_360() {
	Planet planetPositive = new Planet("", 90, 1);
	planetPositive.toOrbit(5);
	assertEquals(90, planetPositive.getAngularPosition(), deltaD);

	Planet planetNegative = new Planet("", -90, 1);
	planetNegative.toOrbit(5);
	assertEquals(-90, planetNegative.getAngularPosition(), deltaD);

    }

}
