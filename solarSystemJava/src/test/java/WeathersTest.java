package test.java;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import main.java.tools.Point;
import main.java.weathers.Drought;

public class WeathersTest {
    Drought drought = new Drought();

    @Test
    public void drougthTrue() {
	assertEquals(true, drought.willHappen(new Point(500, 0), new Point(1000, 0), new Point(2000, 0)));
    }

    @Test
    public void drougthFalse() {
	assertEquals(false, drought.willHappen(new Point(500, 50), new Point(1000, 0), new Point(2000, 0)));
    }

    @Test
    public void optimalCondition() {

    }
}
