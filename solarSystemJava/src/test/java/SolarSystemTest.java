package test.java;

import static org.junit.Assert.assertEquals;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import main.java.solarSystem.Planet;
import main.java.solarSystem.SolarSystem;

public class SolarSystemTest {
    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final PrintStream originalOut = System.out;

    @Before
    public void setUpStreams() {
	System.setOut(new PrintStream(outContent));
    }

    @After
    public void restoreStreams() {
	System.setOut(originalOut);
    }

    @Test
    public void show_planet_information() {
	List<Planet> planet_list = new ArrayList<Planet>();
	planet_list.add(new Planet("Ferengi", 1, 500));
	planet_list.add(new Planet("Vulcano", -5, 1000));
	planet_list.add(new Planet("Betasoide", 3, 2000));

	String initialPositions = "Ferengi > Posicion Angular: 0.0\n" + "Ferengi > Posicion Cartesiana: (500.0, 0.0)\n"
		+ "Vulcano > Posicion Angular: 0.0\n" + "Vulcano > Posicion Cartesiana: (1000.0, 0.0)\n"
		+ "Betasoide > Posicion Angular: 0.0\n" + "Betasoide > Posicion Cartesiana: (2000.0, 0.0)\n";

	SolarSystem.showPlanets(planet_list);
	assertEquals(initialPositions, outContent.toString());
    }

    @Test
    public void orbit_planets() {
	List<Planet> planet_list = new ArrayList<Planet>();
	planet_list.add(new Planet("Ferengi", 1, 500));
	planet_list.add(new Planet("Vulcano", -5, 1000));
	planet_list.add(new Planet("Betasoide", 3, 2000));

	SolarSystem.toOrbit(planet_list, 90);

	String postPositions = "Ferengi > Posicion Angular: 90.0\n" + "Ferengi > Posicion Cartesiana: (0.0, 500.0)\n"
		+ "Vulcano > Posicion Angular: -90.0\n" + "Vulcano > Posicion Cartesiana: (0.0, -1000.0)\n"
		+ "Betasoide > Posicion Angular: 270.0\n" + "Betasoide > Posicion Cartesiana: (0.0, -2000.0)\n";

	SolarSystem.showPlanets(planet_list);
	assertEquals(postPositions, outContent.toString());
    }

}
