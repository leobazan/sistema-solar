package test.java;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import main.java.tools.MathTools;
import main.java.tools.Point;

public class MathToolsTest {
    double delta = 0;
    double max_leg_heigh_to_be_considered_alignment = 44;

    Point pointA = new Point(500, 0);
    Point pointB = new Point(1000, 0);
    Point pointC = new Point(2000, 0);

    @Test
    public void triangleArea() {
	assertEquals(0, MathTools.triangleArea(pointA, pointB, pointC), delta);

	assertEquals(200, MathTools.triangleArea(new Point(0, 0), new Point(20, 0), new Point(0, 20)), delta);

    }

    @Test
    public void isTriangle() {
	assertEquals(false, MathTools.isTriangle(pointA, pointB, pointC));

	assertEquals(true,
		MathTools.isTriangle(new Point(500, max_leg_heigh_to_be_considered_alignment + 1), pointB, pointC));

    }

    @Test
    public void its_alignment() {
	assertEquals(true, MathTools.its_alignment(pointA, pointB, pointC));

	assertEquals(false,
		MathTools.its_alignment(new Point(500, max_leg_heigh_to_be_considered_alignment + 1), pointB, pointC));

    }

    @Test
    public void isInside() {
	assertEquals(true, MathTools.isInside(new Point(0, 0), new Point(10, 0), new Point(0, 10), new Point(1, 1)));
	assertEquals(false, MathTools.isInside(new Point(0, 0), new Point(10, 0), new Point(0, 10), new Point(-1, 0)));

    }

}
