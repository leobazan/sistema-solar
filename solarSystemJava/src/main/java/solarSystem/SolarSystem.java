package main.java.solarSystem;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.NoSuchElementException;

import main.java.tools.MathTools;
import main.java.tools.Point;
import main.java.weathers.Drought;
import main.java.weathers.Event;
import main.java.weathers.OptimalCondition;
import main.java.weathers.Rain;
import main.java.weathers.Weather;

public class SolarSystem {

    static final Drought drought = new Drought();
    static final OptimalCondition optimalCondition = new OptimalCondition();
    static final Rain rain = new Rain();

    public static void advanceDailyPosition(List<Planet> planetList) {
	for (Planet planet : planetList) {
	    planet.advanceDailyPosition();
	}
	;
    }

    public static double planetsArea(List<Planet> planetList) {
	Point pointA = planetList.get(0).getCartesianPosition();
	Point pointB = planetList.get(1).getCartesianPosition();
	Point pointC = planetList.get(2).getCartesianPosition();
	return MathTools.triangleArea(pointA, pointB, pointC);
    }

    public static boolean evaluateEvent(Weather weatherEvent, List<Planet> planetList) {
	Point point_A = planetList.get(0).getCartesianPosition();
	Point point_B = planetList.get(1).getCartesianPosition();
	Point point_C = planetList.get(2).getCartesianPosition();

	return weatherEvent.willHappen(point_A, point_B, point_C);
    }

    private static List<String> cartesianPoints(List<Planet> planetList) {
	List<String> cartesianPoints = new ArrayList<String>();
	cartesianPoints.add(planetList.get(0).getCartesianPositionString());
	cartesianPoints.add(planetList.get(1).getCartesianPositionString());
	cartesianPoints.add(planetList.get(2).getCartesianPositionString());

	return cartesianPoints;
    }

    public static void seeEvents(List<Planet> planetList, int translationDays) {
	List<Event> droughList = new ArrayList<Event>();
	List<Event> optimalConditionList = new ArrayList<Event>();
	List<Event> rainList = new ArrayList<Event>();

	for (int dayNumber = 0; dayNumber < translationDays; dayNumber++) {
	    double area = planetsArea(planetList);
	    List<String> cartesianPoints = cartesianPoints(planetList);

	    if (evaluateEvent(drought, planetList)) {
		droughList.add(new Event("DROUGHT", dayNumber, area, cartesianPoints));
	    } else if (evaluateEvent(rain, planetList)) {
		rainList.add(new Event("RAIN", dayNumber, area, cartesianPoints));

	    } else if (evaluateEvent(optimalCondition, planetList)) {
		optimalConditionList.add(new Event("OPTIMALCONDITION", dayNumber, area, cartesianPoints));
	    }

	    advanceDailyPosition(planetList);
	}

	System.out.println("Cantidad de dias de SEQUIA : " + droughList.size());
	System.out.println("Cantidad de dias de condicion OPTIMA : " + optimalConditionList.size());
	System.out.println("Cantidad de dias de LLUVIA : " + rainList.size());
	Event rainiestDay = rainList.stream().max(Comparator.comparing(Event::getArea))
		.orElseThrow(NoSuchElementException::new);

	System.out.println(
		"Dia que llovio mas : " + rainiestDay.getDay() + ". Area planetaria : " + rainiestDay.getArea());

    }

    // Functions for testing

    public static void toOrbit(List<Planet> planetList, int days) {
	for (int i = 0; i < days; i++) {
	    advanceDailyPosition(planetList);
	}
	;
    }

    public static void showPlanets(List<Planet> planetList) {
	for (Planet planet : planetList) {
	    planet.showPlanet();
	}
    }

    public static int amountOfRainyDays(List<Planet> planetList, int translationDays) {
	return amountOfDays(rain, planetList, translationDays);

    }

    public static int amountOfDroughtDays(List<Planet> planetList, int translationDays) {
	return amountOfDays(drought, planetList, translationDays);
    }

    public static int amountOfDays(Weather weatherEvent, List<Planet> planetList, int translationDays) {
	int event_count = 0;

	for (int dayNumber = 0; dayNumber < translationDays; dayNumber++) {
	    if (evaluateEvent(weatherEvent, planetList)) {
		event_count += 1;
	    }
	    advanceDailyPosition(planetList);
	}
	return event_count;
    }
}
