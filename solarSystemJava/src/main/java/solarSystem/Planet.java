package main.java.solarSystem;

import main.java.tools.MathTools;
import main.java.tools.Point;

public class Planet {
    static final int circleDegrees = 360;
    double angularVelocity;
    double distance;
    double angularPosition;
    String name;
    Point cartesianPosition;

    public Planet(String name, double angularVelocity, double distance) {
	this.angularVelocity = angularVelocity;
	this.distance = distance; // Se tomara matematicamente como el modulo con respecto al origen(el sol)
	this.angularPosition = 0;
	this.cartesianPosition = new Point(distance, 0);
	this.name = name;
    }

    public void advanceDailyPosition() {
	increaseAngularPosition();
	updateCartesianPosition();
    }

    public void increaseAngularPosition() {
	angularPosition += angularVelocity;
	if (Math.abs(angularPosition) >= circleDegrees) {
	    if (angularPosition > 0) {
		angularPosition -= circleDegrees;
	    } else {
		angularPosition += circleDegrees;
	    }
	}
    }

    private void updateCartesianPosition() {
	cartesianPosition = MathTools.polarToCartesian(distance, angularPosition);
    }

    public void toOrbit(int days) {
	for (int i = 0; i < days; i++) {
	    advanceDailyPosition();
	}
    }

    public void showPlanet() {
	System.out.println(name + " > Posicion Angular: " + angularPosition);
	System.out.println(name + " > Posicion Cartesiana: " + getCartesianPositionString());

    }

    public String getCartesianPositionString() {
	return "(" + cartesianPosition.getX() + ", " + cartesianPosition.getY() + ")";
    }

    public Point getCartesianPosition() {
	return cartesianPosition;
    }

    public double getAngularPosition() {
	return angularPosition;
    }

    public String getName() {
	return name;
    }
}
