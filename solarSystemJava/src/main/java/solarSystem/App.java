package main.java.solarSystem;

import java.util.ArrayList;
import java.util.List;

public class App {
    public static void main(String[] args) {

	final List<Planet> planetList = new ArrayList<Planet>();
	planetList.add(new Planet("Ferengi", 1, 500));
	planetList.add(new Planet("Vulcano", -5, 1000));
	planetList.add(new Planet("Betasoide", 3, 2000));

	int translation_days = 365 * 10;

	SolarSystem.seeEvents(planetList, translation_days);

    }
}
