package main.java.weathers;

import main.java.tools.MathTools;
import main.java.tools.Point;

public class OptimalCondition implements Weather {

    @Override
    public boolean willHappen(Point pointA, Point pointB, Point pointC) {
	return MathTools.its_alignment(pointA, pointB, pointC) && !MathTools.its_alignment(sunPosition, pointB, pointC);

    }

    // its_astronomical_alignment(pointA, pointB, pointC) and is_triangle(sun,
    // pointB, pointC)

}
