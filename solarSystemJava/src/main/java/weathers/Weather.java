package main.java.weathers;

import main.java.tools.Point;

public interface Weather {
    final static Point sunPosition = new Point(0, 0);

    public abstract boolean willHappen(Point pointA, Point pointB, Point pointC);

}
