package main.java.weathers;

import java.util.ArrayList;
import java.util.List;

public class Event {
    final int day;
    final String weatherType;
    final double area;
    List<String> cartesianPosition = new ArrayList<String>();

    public Event(String weatherType, int day, double area, List<String> cartesianPosition) {
	this.day = day;
	this.weatherType = weatherType;
	this.area = area;
	this.cartesianPosition = cartesianPosition;
    }

    public int getDay() {
	return day;
    }

    public String weathertype() {
	return weatherType;
    }

    public double getArea() {
	return area;
    }

    public List<String> getCartesianPosition() {
	return cartesianPosition;
    }

}
