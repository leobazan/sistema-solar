package main.java.weathers;

import main.java.tools.MathTools;
import main.java.tools.Point;

public class Rain implements Weather {

    @Override
    public boolean willHappen(Point pointA, Point pointB, Point pointC) {
	return MathTools.isTriangle(pointA, pointB, pointC)
		&& MathTools.isInside(pointA, pointB, pointC, sunPosition);
    }

}
