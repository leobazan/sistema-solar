package main.java.weathers;

import main.java.tools.MathTools;
import main.java.tools.Point;

public class Drought implements Weather {
    @Override
    public boolean willHappen(Point pointA, Point pointB, Point pointC) {
	return MathTools.its_alignment(pointA, pointB, pointC) && MathTools.its_alignment(sunPosition, pointB, pointC);
    }

}
