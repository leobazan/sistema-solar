package main.java.tools;

import java.util.List;

public class MathTools {
    public final static int precisionInDecimalsOfDegrees = 10;

    // When evaluating points (they are points because these planets have no
    // diameter) so far apart, the area of ​​the triangle will almost never give 0.
    // That is why I define a margin of error in the area.
    // (Another solution to not confuse a triangle with an alignment could be
    // ASSIGNING A DIAMETER TO THE PLANETS)
    public final static int presicionTriangleArea = 22000;

    public static Point polarToCartesian(double module, double angular_position) {
	double scale = Math.pow(10, precisionInDecimalsOfDegrees);
	double x = module * Math.round(Math.cos(Math.toRadians(angular_position)) * scale) / scale;
	double y = module * Math.round(Math.sin(Math.toRadians(angular_position)) * scale) / scale;
	return new Point(x, y);
    }

    public static boolean isTriangle(Point pointA, Point pointB, Point pointC) {
	return triangleArea(pointA, pointB, pointC) > presicionTriangleArea;
    }

    public static double triangleArea(Point pointA, Point pointB, Point pointC) {
	return Math.abs((pointA.getX() - pointC.getX()) * (pointB.getY() - pointA.getY())
		- (pointA.getX() - pointB.getX()) * (pointC.getY() - pointA.getY())) * 0.5;

    }

    public static boolean isInside(Point pointA, Point pointB, Point pointC, Point pointP) {
	double area_A = triangleArea(pointA, pointB, pointC);
	double area_A1 = triangleArea(pointP, pointB, pointC);
	double area_A2 = triangleArea(pointP, pointA, pointC);
	double area_A3 = triangleArea(pointP, pointA, pointB);

	return (area_A == area_A1 + area_A2 + area_A3);
    }

    public static boolean its_alignment(Point pointA, Point pointB, Point pointC) {
	return !isTriangle(pointA, pointB, pointC);
    }

    public static double maxArea(List<Double> areas) {
	return areas.stream().mapToDouble(i -> i).max().getAsDouble();
    }

}