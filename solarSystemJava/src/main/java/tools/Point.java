package main.java.tools;

public class Point {
    double x;
    double y;

    public Point(double coordinateX, double coordinateY) {
	x = coordinateX;
	y = coordinateY;
    }

    public double getX() {
	return x;
    }

    public double getY() {
	return y;
    }
}
