require_relative 'math.rb'
# Para que un astro este alineado, necesita estar en el mismo grado o en el grado opuesto
# VER COMO DETERMINAR ALINEAMIENTO
# Caso Planetas Alineados consigo y tambien con el sol

# Para que los astros esten alineados no tienen que formar un triangulo
# Para formar un triangulo, se debe comprobar que cumplan con la ley de los triangulos (la
# suma dos de sus lados tiene que ser mayor al lado restante)

# 1)SEQUIA : Todos los astros alineados (Incluyendo el sol)
# 2)LLUVIA: Planetas forman un triangulo con el sol adentro de el triangulo
# 3)LLUVIA MAXIMA: Planetas forman un triangulo (TRIANGULO MAXIMO) con el sol adentro de el triangulo
# 4)CONDICIONES IDEALES: Planetas alineados entre si pero NO CON EL SOL

# Crear un simulador de planetas en movimiento el cual haga que se muevan cada dia e indiquen su
# posicion actual segun el dia
# En un periodo de 10 años (365 dias por 10) detectar SEQUIA, LLUVIA, LLUVIA MAXIMA; CONDICIONES IDEALES

# Cuando se guarden estos casos especiales, indicar dia especifico y cantidad en la que ocurren (un acumulador
# por caso)

def rain?(planetA, planetB, planetC)
  sun = Point.new(0, 0)
  is_triangle(planetA, planetB, planetC) and is_inside(planetA, planetB, planetC, sun)
end

# TODO
def max_rain?
end

def drought(pointA, pointB, pointC)
  its_astronomical_alignment(pointA, pointB, pointC) and its_astronomical_alignment( Point.new(0,0), pointB, pointC)
end

def optimal_conditions(pointA, pointB, pointC)
  sun = Point.new(0, 0)
  its_astronomical_alignment(pointA, pointB, pointC) and is_triangle(sun, pointB, pointC)
end