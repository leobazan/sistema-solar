require_relative "math.rb"
# DEFINICIONES:
class Planet
  CIRCLE_degrees = 360
  attr_reader :degrees, :distance, :angular_position, :cartesian_position, :degrees_in_radians

  def initialize(degrees, distance)
    @degrees = degrees.to_f
    @distance = distance  # Es el modulo con respecto al origen(el sol)
    @angular_position = 0.0
    @degrees_in_radians = to_rad(@angular_position)
    @cartesian_position = Point.new(distance, 0.0)
  end

  def to_rad(degrees)
    degrees * Math::PI / 180.0
  end

  def advance_daily_position
    increase_angular_position
    update_cartesian_position
  end

  def increase_angular_position
    @angular_position += @degrees
    if (@angular_position.abs >= CIRCLE_degrees)
      if @angular_position > 0
        @angular_position -= CIRCLE_degrees
      else
        @angular_position += CIRCLE_degrees
      end
    end
    update_degrees_in_radians
  end

  def update_degrees_in_radians
    @degrees_in_radians = to_rad(@angular_position)
  end

  def update_cartesian_position
    @cartesian_position = polar_to_cartesian(@distance, @degrees_in_radians)
  end

  def polar_to_cartesian(module_, degrees_in_radians)
    x = module_ * Math.cos(degrees_in_radians).round(7)
    y = module_ * Math.sin(degrees_in_radians).round(7)
    Point.new(x, y)
  end

  def to_orbit(days)
    days.times do
      advance_daily_position
    end
  end

  def see_cartesian_position
    "( #{@cartesian_position.x}, #{@cartesian_position.y} )"
  end
end
