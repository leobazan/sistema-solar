
class Point
  attr_accessor :x, :y

  def initialize(x, y)
    @x = x
    @y = y
  end
end

# Funciones matematicas
def triangle_area(pointA, pointB, pointC)
  ((pointA.x * (pointB.y - pointC.y) + pointB.x * (pointC.y - pointA.y) + pointC.x * (pointA.y - pointB.y)) / 2.0).abs
end

def is_triangle(pointA, pointB, pointC)
  precision_margin = 40000
  # Como no hay datos del diametro de los planetas, y solo se estan evaluando puntos
  # (diametro 0, cosa irreal para cualquier planeta) y sumado al hecho de grandes distancias entre
  # lados del triangulo, el area de dicho triangulo casi nunca dara 0, por eso se utilizara un margen
  # de presicion ( OTRA SOLUCION ES PONER DIAMETRO A LOS PLANETAS)
  triangle_area(pointA, pointB, pointC).round(1) > precision_margin
end

def is_inside(pointA, pointB, pointC, pointP)
  # Calculate area of triangle ABC
  area_A = triangle_area(pointA, pointB, pointC)

  # Calculate area of triangle PBC
  area_A1 = triangle_area(pointP, pointB, pointC)

  # Calculate area of triangle PAC
  area_A2 = triangle_area(pointP, pointA, pointC)

  # Calculate area of triangle PAB
  area_A3 = triangle_area(pointP, pointA, pointB)

  # Check if sum of A1, A2 and A3
  # is same as A
  if (area_A == area_A1 + area_A2 + area_A3)
    true
  else
    false
  end
end

def its_astronomical_alignment(pointA, pointB, pointC)
  not is_triangle(pointA, pointB, pointC)
end


