require_relative "planeta.rb"
require_relative "math.rb"
require_relative "weather.rb"

a = Point.new(0, 0)
b = Point.new(2, 2)
c = Point.new(4, 4.01)

# Sistema Solar

def show_planet(planet)
  puts "Posicion Angular #{planet.angular_position}"
  puts "Posicion Cartesiana (#{planet.cartesian_position.x}, #{planet.cartesian_position.y})"
end

def advance_daily_position(planet_list)
  planet_list.each do |planet|
    planet.advance_daily_position
  end
end

def check_events(planet_list, day)
  point_A = planet_list[0].cartesian_position
  point_B = planet_list[1].cartesian_position
  point_C = planet_list[2].cartesian_position


  if optimal_conditions(point_A, point_B, point_C)
    puts "Alineacion con el sol,  DIA : #{day}"
    return true
  end
  return false
  # if optimal_conditions(point_A, point_B, point_C)
  #   #puts "Condiciones Optimas, esto ocurre en el dia numero #{day}"
  #   #return true
  # end
end

def show_planets(planet_list)
      planet_list.each do |planet|
        show_planet(planet)
      end
end

def see_events(planet_list, translation_days)
  planets_area = []
  optimal_condition_count = 0
  translation_days.times do |day_number|

    if check_events(planet_list, day_number)

      point_A = planet_list[0].cartesian_position
      point_B = planet_list[1].cartesian_position
      point_C = planet_list[2].cartesian_position
      show_planets(planet_list)
      planets_area << triangle_area(point_A, point_B, point_C)
      optimal_condition_count += 1
    end

    advance_daily_position(planet_list)
  end
  puts "\n\nArea Minima : #{planets_area.min}\nArea Maxima : #{planets_area.max}\nCantidad de eventos #{optimal_condition_count}"
end

# Implementacion
#
#

ferengi = Planet.new(1, 500)
betasoide = Planet.new(3, 2000)
vulcano = Planet.new(-5, 1000)

planet_list = [ferengi, betasoide, vulcano]
translation_days = 365 * 10

see_events(planet_list, translation_days)



# 91.times {
#   puts ferengi.see_cartesian_position;
#   ferengi.advance_daily_position 
# }